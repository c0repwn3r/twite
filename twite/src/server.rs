use std::{net::{TcpStream}, io::{Read, self, ErrorKind, Write, Cursor}, str, collections::HashMap};

use base64::{Engine as _, engine::general_purpose};
use sha1::{Sha1, Digest};
use url::{Url};

use crate::{Error, stream::WsStream};

pub trait WsServer {
    fn upgrade(&mut self) -> Result<WsStream, Error>;
}

impl WsServer for TcpStream {
    fn upgrade(&mut self) -> Result<WsStream, Error> {
        let mut buf = Vec::new();
        let mut buf_bytes = [0u8; 1024];
        loop {
            let read = match self.read(&mut buf_bytes) {
                Ok(s) => s,
                Err(e) => return Err(Error::IoError(e))
            };

            if read == 0 {
                break;
            }

            buf.extend_from_slice(&buf_bytes[0..read]);

            if buf.ends_with(&[0x0d, 0x0a, 0x0d, 0x0a]) {
                break;
            }

            let mut should_break = false;
            for i in 0..buf.len() {
                let slice = &buf[0..i+1];
                if slice.ends_with(&[0x0d, 0x0a, 0x0d, 0x0a]) {
                    should_break = true;
                    break;
                }
            }
            if should_break { break; }
        }
        let orig_message = match str::from_utf8(&buf.as_slice()) {
            Ok(v) => v,
            Err(e) => return Err(Error::IoError(io::Error::new(ErrorKind::Other, format!("Invalid UTF-8 string: {}", e)))),
        };
        let message: Vec<&str> = orig_message.split("\n").collect();
        let mut fields: HashMap<String, String> = HashMap::new();
        message.iter().skip(1).for_each(|s| {
            if s.trim().is_empty() { return; }
            let values: Vec<&str> = s.split(": ").collect();
            let name = values[0].trim().to_lowercase();
            let value = if name != "sec-websocket-key" {
                values[1].trim().to_lowercase()
            } else {
                values[1].trim().to_string()
            };
            fields.insert(name, value);
        });
        let request: Vec<&str> = message[0].trim().split(" ").collect();
        inn::assert_code!(request[0] == "GET", Error::InvalidRequest("Error at GET".to_string()));
        inn::assert_code!(request[2].starts_with("HTTP/"), Error::InvalidRequest("Error at HTTP/".to_string()));
        inn::assert_code!(request[2][5..].parse::<f64>().unwrap() >= 1.1, Error::InvalidRequest("Error at HTTP version".to_string()));
        let host = fields.get("host").ok_or(Error::HttpRequest(orig_message.to_string()))?; 
        inn::assert_code!(Url::parse(&["ws://".to_string(), host.clone()].concat()).is_ok(), Error::HttpRequest(orig_message.to_string()));
        inn::assert_code!(fields.get("upgrade").ok_or(Error::HttpRequest(orig_message.to_string()))? == "websocket", Error::InvalidRequest("Invalid Upgrade value".to_string()));
        inn::assert_code!(fields.get("connection").ok_or(Error::HttpRequest(orig_message.to_string()))? == "upgrade" || fields.get("connection").ok_or(Error::HttpRequest(orig_message.to_string()))? == "keep-alive, upgrade", Error::InvalidRequest("Invalid Connection value".to_string()));
        inn::assert_code!(fields.get("sec-websocket-version").ok_or(Error::HttpRequest(orig_message.to_string()))? == "13", Error::InvalidRequest("Invalid websocket version".to_string()));
        let key = fields.get("sec-websocket-key").ok_or(Error::HttpRequest(orig_message.to_string()))?.clone();
        let key = [key, "258EAFA5-E914-47DA-95CA-C5AB0DC85B11".to_string()].concat();
        let mut hasher = Sha1::new();
        hasher.update(key);
        let key = general_purpose::STANDARD.encode(hasher.finalize().as_slice());

        let msg = format!("HTTP/1.1 101 Switching Protocols\r
Upgrade: websocket\r
Connection: Upgrade\r
Sec-WebSocket-Accept: {}\r\n\r\n", key);
        if let Err(e) = self.write_all(msg.as_bytes()) {
            return Err(Error::IoError(e));
        }

        Ok(WsStream {
            url: Url::parse(&["ws://".to_string(), host.clone()].concat()).unwrap(),
            key: None,
            read: Cursor::new(Vec::new()).chain(self.try_clone().unwrap()),
            write: self.try_clone().unwrap(),
            closing: false,
            fragment: None,
            client: false,
        })
    }
}

mod inn {
    #[macro_export]
    macro_rules! assert_code {
        ($equal:expr, $type:expr $(,)?) => {
            if !$equal {
                return Err($type);
            }
        };
    }
    pub(crate) use assert_code;
}
