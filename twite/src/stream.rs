use std::{io::{Cursor, Write, Chain, Read}, net::TcpStream, str};

use url::Url;

use crate::{frame::{MessageType, write_frame, read_frame}, CloseReason, Error};

pub struct WsStream {
    pub url: Url,
    pub key: Option<[u8; 16]>,
    pub read: Chain<Cursor<Vec<u8>>,TcpStream>,
    pub write: TcpStream,
    pub closing: bool,
    pub(crate) fragment: Option<(MessageType, Vec<u8>)>,
    pub(crate) client: bool,
}

impl Clone for WsStream {
    fn clone(&self) -> Self {
        let read = self.read.get_ref();
        WsStream {
            url: self.url.clone(),
            key: self.key,
            read: read.0.clone().chain(read.1.try_clone().unwrap()),
            write: self.write.try_clone().unwrap(),
            closing: self.closing,
            fragment: self.fragment.clone(),
            client: self.client,
        }
    }
}

impl WsStream {
    pub fn send(&mut self, frame_type: MessageType, payload: Vec<u8>) -> Result<(), Error> {
        let message = write_frame(frame_type, payload, self.client);
        match self.write.write_all(&message) {
            Ok(_) => {}
            Err(e) => return Err(Error::IoError(e)),
        }
        Ok(())
    }
    pub fn pong(&mut self, payload: Vec<u8>) -> Result<(), Error> {
        let message = write_frame(MessageType::Pong, payload, self.client);
        match self.write.write_all(&message) {
            Ok(_) => {}
            Err(e) => return Err(Error::IoError(e)),
        }
        Ok(())
    }
    pub fn close(&mut self, status: Option<CloseReason>) -> Result<(), Error> {
        let mut payload = Vec::new();
        if let Some(status) = status {
            payload.append(&mut (status as u16).to_be_bytes().to_vec());
        }
        let message = write_frame(MessageType::Close, payload, self.client);
        if let Err(e) = self.write.write_all(&message) {
            return Err(Error::IoError(e));
        }
        Err(Error::ConnectionClosed)
    }
    pub fn read(&mut self) -> Result<(MessageType, Vec<u8>), Error> {
        let result = read_frame(&mut self.fragment, &mut self.read);
        match result {
            Ok(s) => {
                match s.0 {
                    MessageType::Text => {
                        match str::from_utf8(&s.1) {
                            Ok(_) => {}
                            Err(_) => {
                                self.close(Some(CloseReason::UnexpectedData))?;
                            }
                        }
                    }
                    MessageType::Ping => {
                        self.pong(s.1.clone())?;
                    }
                    _ => {}
                }
                Ok(s)
            },
            Err(e) => match e {
                Error::IncorrectFormat => {
                    self.close(Some(CloseReason::ProtocolError))?;
                    Err(Error::ConnectionClosed)
                }
                Error::InvalidOpcode => {
                    self.close(Some(CloseReason::ProtocolError))?;
                    Err(Error::ConnectionClosed)
                }
                _ => Err(e)
            }
        }
    }
}
