use std::{io::Read};

use rand::Rng;

use crate::Error;

#[derive(Debug, Clone, Copy)]
pub enum MessageType {
    Continuation = 0x0,
    Text = 0x1,
    Binary = 0x2,
    Close = 0x8,
    Ping = 0x9,
    Pong = 0xA,
}

pub fn write_frame(frame_type: MessageType, payload: Vec<u8>, is_client: bool) -> Vec<u8> {
    let mut payload_length: Vec<u8> = if payload.len() <= 125 {
        if is_client {
            vec![0x80 | payload.len() as u8]
        } else {
            vec![payload.len() as u8]
        }
    } else if payload.len() <= 65535 {
        let mut length = if is_client {
            vec![0x80 | 126]
        } else {
            vec![126]
        };
        length.append(&mut (payload.len() as u16).to_be_bytes().to_vec());
        length
    } else {
        let mut length = if is_client {
            vec![0x80 | 127]
        } else {
            vec![127]
        };
        length.append(&mut (payload.len() as u64).to_be_bytes().to_vec());
        length
    };
    let mut message: Vec<u8> = vec![0b10000000 | frame_type as u8];
    message.append(&mut payload_length);
    if is_client {
        let mut rng = rand::thread_rng();
        let key: [u8; 4] = rng.gen();
        message.append(&mut key.to_vec());
        let mut transform: Vec<u8> = Vec::with_capacity(payload.len());

        for (i, byte) in payload.iter().enumerate() {
            let j = i % 4;
            transform.push(byte ^ key[j]);
        }
        message.append(&mut transform);
        message
    } else {
        message.append(&mut payload.to_vec());
        message
    }
}

fn read_fragment<R: Read>(stream: &mut R) -> Result<(MessageType, Vec<u8>, u8), Error> {
    let mut byte = [0u8; 1];
    match stream.read(&mut byte) {
        Ok(r) => {
            if r == 0 {
                return Err(Error::InsufficientData);
            }
        }
        Err(e) => { return Err(Error::IoError(e)) },
    };
    let fin = (1 << 7 & byte[0]) >> 7;
    let rsv1 = (1 << 6 & byte[0]) >> 4;
    let rsv2 = (1 << 5 & byte[0]) >> 4;
    let rsv3 = (1 << 4 & byte[0]) >> 4;
    if rsv1 | rsv2 | rsv3 != 0 {
        return Err(Error::IncorrectFormat);
    }
    let opcode = 0b1111 & byte[0];
    let opcode = match opcode {
        0x0 => MessageType::Continuation,
        0x1 => MessageType::Text,
        0x2 => MessageType::Binary,
        0x8 => MessageType::Close,
        0x9 => MessageType::Ping,
        0xA => MessageType::Pong,
        _ => { return Err(Error::InvalidOpcode); },
    };

    match stream.read(&mut byte) {
        Ok(r) => {
            if r == 0 {
                return Err(Error::InsufficientData);
            }
        }
        Err(e) => { return Err(Error::IoError(e)) },
    };
    let mask = 0x80 & byte[0];
    let payload_length = 0x7f & byte[0];
    let payload_length: usize = if payload_length < 126 {
        payload_length as usize
    } else if payload_length == 126 {
        let mut length = [0u8; 2];
        match stream.read(&mut length) {
            Ok(r) => {
                if r == 0 {
                    return Err(Error::InsufficientData);
                }
            }
            Err(e) => { return Err(Error::IoError(e)) },
        };
        ((length[0] as u16) << 8u16 | (length[1] as u16)) as usize
    } else {
        let mut length = [0u8; 8];
        match stream.read(&mut length) {
            Ok(r) => {
                if r == 0 {
                    return Err(Error::InsufficientData);
                }
            }
            Err(e) => { return Err(Error::IoError(e)) },
        };
        ((length[0] as u64) << 56 | (length[1] as u64) << 48 |
         (length[2] as u64) << 40 | (length[3] as u64) << 32 |
         (length[4] as u64) << 24 | (length[5] as u64) << 16 |
         (length[6] as u64) << 8  | (length[7] as u64)) as usize
    };
    match opcode {
        MessageType::Close | MessageType::Ping | MessageType::Pong => {
            if payload_length > 125 {
                return Err(Error::IncorrectFormat);
            }
        }
        _ => {}
    }
    if mask != 0 {
        let mut key = [0u8; 4];
        match stream.read_exact(&mut key) {
            Ok(_) => {}
            Err(e) => { return Err(Error::IoError(e)) },
        };
        let mut transform = vec![0u8; payload_length];
        match stream.read_exact(&mut transform) {
            Ok(_) => {}
            Err(e) => { return Err(Error::IoError(e)) },
        };
        let mut message = Vec::new();

        for (i, byte) in transform.iter().enumerate() {
            let j = i % 4;
            message.push(byte ^ key[j]);
        }
        Ok((opcode, message, fin))
    } else {
        let mut message = vec![0u8; payload_length];
        match stream.read_exact(&mut message) {
            Ok(_) => {}
            Err(e) => { return Err(Error::IoError(e)) },
        };
        Ok((opcode, message, fin))
    }
}

pub fn read_frame<R: Read>(fragment: &mut Option<(MessageType, Vec<u8>)>, stream: &mut R) -> Result<(MessageType, Vec<u8>), Error> {
    // read guaranteed initial fragment
    match read_fragment(stream) {
        Ok((first_frame_type, mut payload, fin)) => {
            if let MessageType::Continuation = first_frame_type {
                if fragment.is_none() {
                    return Err(Error::IncorrectFormat);
                }
            }
            if fin == 0 {
                match first_frame_type {
                    MessageType::Ping | MessageType::Pong | MessageType::Close => {
                        return Err(Error::IncorrectFormat);
                    }
                    _ => {}
                }
                let mut running_payload = Vec::new();
                if let Some(fragment) = fragment {
                    running_payload.append(&mut fragment.1);
                    running_payload.append(&mut payload);
                } else {
                    running_payload.append(&mut payload);
                }
                loop {
                    // more fragments in the message
                    match read_fragment(stream) {
                        Ok((frame_type, mut payload, fin)) => {
                            match frame_type {
                                MessageType::Continuation => {
                                    if fin == 1 {
                                        running_payload.append(&mut payload);
                                        *fragment = None;
                                        return Ok((first_frame_type, running_payload));
                                    } else if fin == 0 {
                                        running_payload.append(&mut payload);
                                    } else {
                                        return Err(Error::IncorrectFormat);
                                    }
                                }
                                MessageType::Ping | MessageType::Pong | MessageType::Close => {
                                    if let Some(frag) = fragment {
                                        *fragment = Some((frag.0, running_payload.clone()));
                                    } else {
                                        *fragment = Some((first_frame_type, running_payload));
                                    }
                                    return Ok((frame_type, payload));
                                }
                                _ => {
                                    return Err(Error::IncorrectFormat);
                                }
                            }
                        }
                        Err(e) => return Err(e)
                    }
                }
            } else {
                if let Some(in_fragment) = fragment {
                    let mut new_payload = in_fragment.1.clone();
                    new_payload.append(&mut payload);
                    let fragment_type = in_fragment.0;
                    *fragment = None;
                    Ok((fragment_type, new_payload))
                } else {
                    Ok((first_frame_type, payload))
                }
            }
        }
        Err(e) => Err(e)
    }
}
