use std::{net::{TcpListener, SocketAddr, Incoming, TcpStream, ToSocketAddrs}, io, collections::HashMap, thread, sync::{Mutex, Arc, mpsc}};

use crossbeam_channel::{unbounded, Receiver, Sender};
pub use twite;

use bevy::{prelude::*, ecs::event::ManualEventReader};
use twite::{server::WsServer, frame::MessageType, Error};

pub struct TwiteServerPlugin;

impl Plugin for TwiteServerPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<ServerEvent>();
        app.add_systems(Startup, Self::init_server);
        app.add_systems(PostUpdate, Self::event_listener);
    }
}

#[derive(Event, Clone, Debug)]
pub enum ServerEvent {
    Connection(SocketAddr),
    HttpRequest(String),
    Close(SocketAddr),
    Send(SocketAddr, MessageType, Vec<u8>),
    Recv(SocketAddr, MessageType, Vec<u8>),
    Broadcast(MessageType, Vec<u8>),
}

pub struct TwiteServer {
    listener: TcpListener,
}

#[derive(Resource, Clone)]
pub struct Clients(Arc<Mutex<HashMap<SocketAddr, mpsc::Sender<ServerEvent>>>>);

impl TwiteServer {
    pub fn bind<A: ToSocketAddrs>(addr: A) -> io::Result<Self> {
        let listener = TcpListener::bind(addr)?;
        Ok(TwiteServer {
            listener,
        })
    }
    pub fn incoming(&self) -> Incoming<'_> {
        self.listener.incoming()
    }
    pub fn accept(&self) -> io::Result<(TcpStream, SocketAddr)> {
        self.listener.accept()
    }
}

#[derive(Resource)]
pub struct TwiteServerConfig {
    pub port: u32,
}

#[derive(Resource)]
pub struct GBReceiver<A>(Receiver<A>);
#[derive(Resource)]
pub struct GBSender<A>(Sender<A>);

impl TwiteServerPlugin {
    pub fn init_server(server: ResMut<TwiteServerConfig>, mut commands: Commands) {
        let server = TwiteServer::bind(format!("127.0.0.1:{}", server.port.to_string())).unwrap();
        let (game_sender, game_receiver) = unbounded::<ServerEvent>();
        let clients = Clients(Arc::new(Mutex::new(HashMap::new())));
        commands.insert_resource(clients.clone());
        commands.insert_resource(GBReceiver(game_receiver.clone()));
        commands.insert_resource(GBSender(game_sender.clone()));
        let clients = clients.0.clone();
        thread::spawn(move || {
            loop {
                let (mut stream, addr) = server.accept().unwrap();
                let stream = match stream.upgrade() {
                    Ok(s) => s,
                    Err(e) => match e {
                        Error::HttpRequest(s) => {
                            game_sender.send(ServerEvent::HttpRequest(s)).unwrap();
                            continue;
                        }
                        e => panic!("{:?}", e)
                    },
                };
                let (sender, receiver) = mpsc::channel();
                {
                    clients.lock().unwrap().insert(addr, sender);
                }
                let game_sender = game_sender.clone();
                game_sender.send(ServerEvent::Connection(addr)).expect("Could not send connection event");
                thread::spawn(move || {
                    let mut c_stream = stream.clone();
                    thread::spawn(move || {
                        for event in receiver.iter() {
                            match event {
                                ServerEvent::Send(_, message_type, data) => {
                                    c_stream.send(message_type, data).unwrap();
                                }
                                ServerEvent::Close(_) => {
                                    c_stream.close(None).unwrap();
                                    break;
                                }
                                _ => {}
                            }
                        }
                    });
                    let mut c_stream = stream.clone();
                    loop {
                        let (message_type, data) = stream.clone().read().unwrap();
                        if let MessageType::Close = message_type {
                            match c_stream.close(None) {
                                Ok(_) => {}
                                Err(e) => {
                                    match e {
                                        Error::ConnectionClosed => {}
                                        e => panic!("{:?}", e),
                                    }
                                }
                            }
                            game_sender.send(ServerEvent::Close(addr)).expect("Could not send event");
                            break;
                        } else {
                            game_sender.send(ServerEvent::Recv(addr, message_type, data)).expect("Could not send event");
                        }
                    }
                });
            }
        });
    }
    pub fn event_listener(clients: Res<Clients>,
                          game_receiver: Res<GBReceiver<ServerEvent>>,
                          mut event_reader: Local<ManualEventReader<ServerEvent>>,
                          mut events: ResMut<Events<ServerEvent>>) {
        for event in event_reader.read(&events) {
            match event {
                ServerEvent::Send(addr, _, _) => {
                    clients.0.lock().unwrap().get_mut(addr).unwrap().send(event.clone()).expect("Could not send event");
                }
                ServerEvent::Close(addr) => {
                    clients.0.lock().unwrap().get_mut(addr).unwrap().send(event.clone()).expect("Could not send event");
                }
                ServerEvent::Broadcast(message_type, data) => {
                    for client in clients.0.lock().unwrap().iter() {
                        client.1.send(ServerEvent::Send(*client.0, *message_type, data.clone())).expect("Could not send event");
                    }
                }
                _ => {}
            }
        }
        
        if let Ok(event) = game_receiver.0.try_recv() {
            events.send(event);
        }
    }
}
